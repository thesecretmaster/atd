module ATD::Refinements
  # @!visibility private
  refine Hash do
    # Not only merges two hashes, but also merges the hashes that may be nested in.
    #
    # For example:
    #   {a: {b: "c"}}
    # Is a nested hash
    def deep_merge(second)
      merger = proc do |_, v1, v2|
        if v1.is_a?(Hash) && v2.is_a?(Hash) then v1.merge(v2, &merger)
        elsif v1.is_a?(Array) && v2.is_a?(Array) then v1 | v2
        elsif [:undefined, nil, :nil].include?(v2) then v1
        else v2
        end
      end
      merge(second.to_h, &merger)
    end

    def include_in_key?(search)
      each do |key, val|
        return val if key.is_a?(Array) && key.include?(search)
      end
    end
  end

  # This method only exists for the test suite, specifically {ATDTest#test_route_creation}.
  # @!visibility private
  refine Object do
    # Returns the instance variables of a class
    def class_instance_variables
      instance_variables.map { |var| [var, instance_variable_get(var)] }.to_h
    end
  end

  refine Array do
    def where(criteria)
      select do |element|
        criteria.all? do |criterion, expected_value|
          Array(element.public_send(criterion)).include?(expected_value)
        end
      end
    end
  end
end
