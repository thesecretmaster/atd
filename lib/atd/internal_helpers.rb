module InternalHelpers
  def asset(name)
    return "" if name.to_s.empty?
    target_location = caller_locations.reject do |caller_location|
      File.dirname(caller_location.absolute_path.to_s).include? File.expand_path("..", __dir__)
    end
    "#{File.dirname(target_location[0].absolute_path)}/assets/#{name}"
  end
end
