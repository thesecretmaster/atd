module ATD
  # This module holds everything related to the compilation of routes.
  module Compilation
    include InternalHelpers

    def respond_to_missing?(method, include_private = false)
      ATD::Compilation.compilers.key?(method.to_sym) || super
    end

    def method_missing(method, *args, &block)
      return super unless ATD::Compilation.compilers.key?(method.to_sym)
      filename = args.pop
      ATD::Compilation.compilers[method].call(ATD::Compilation.parse(ATD::Compilation.compilers, filename, File.read(asset(filename)), Hash(args).merge(run: false)), *args)
    end

    class << self
      attr_accessor :compilers, :precompilers

      # This method is responsible for live compilation. It takes an ATD::Route as input, and returns either
      # the filename if Route.output is a file or the Route.output string if Route.output is a string.
      # It will also take the file and call the corresponding compilation method on it.
      def compile(route, *opts)
        { content: parse(@compilers, route.filename, route.output, opts), name: route.filename }
      end

      # This method is responsible for precompilation. It takes an ATD::Route as input and compilation options. It
      # sets the routes filename and output properties to be the filename and the contents of that file or, if route.output
      # doesn't correspond to a valid file, it sets both to be route.output. It returns the precompiled version of the file
      # regardless, and sets route.ouput to be the precompiled version unless the opt precompile: false is passed.
      def precompile(route, *opts)
        opts = Hash(opts[0]) unless opts.is_a? Hash
        opts = opts.merge(route.args)
        # route.output should always be a String, File, or NilClass
        name = route.output.to_s
        name = File.basename(route.output) if route.output.is_a?(File)
        # name should always be a String
        content = if route.output.is_a?(File) || (route.output.is_a?(String) && !route.output.empty? && File.exist?(route.output) && !Dir.exist?(route.output))
                    File.read(route.output)
                  elsif !route.output.nil?
                    route.output
                  else
                    ""
                  end
        # content should always be a String
        route.output = parse(@precompilers, name, content, opts) unless opts[:precompile] == false
        route.filename = name
        { content: parse(@precompilers, name, content, opts), name: route.filename }
      end

      def parse(type, name, contents, *opts)
        return contents if name.include?("\n")
        extensions = name.to_s.split(".")
        extensions.shift
        extensions.each do |extension|
          if type.key? extension.to_sym
            contents = type[extension.to_sym].call(contents, *opts) if Hash(opts.last)[:run] != false
            extensions -= [extension]
          end
        end
        contents
      end
    end
  end
end

ATD::Compilation.compilers = {}
ATD::Compilation.precompilers = {}

def to_compile(name, &block)
  warn "You have overriden the #{name.to_sym} compiler" if ATD::Compilation.compilers.key? name.to_sym
  ATD::Compilation.compilers[name.to_sym] = block
end

def to_precompile(name, &block)
  warn "You have overriden the #{name.to_sym} precompiler" if ATD::Compilation.compilers.key? name.to_sym
  ATD::Compilation.precompilers[name.to_sym] = block
end

class Object
  include ATD::Compilation
end
