require_relative 'test_helper'

describe ATD::App do
  include Rack::Test::Methods

  def app
    Name.new
  end

  def test_app_creation
    assert_instance_of Class, ATD.new("Name2")
  end

  def test_rack_template
    assert Name.new.respond_to?("call")
  end

  def test_template_is_rack_app
    responce = Name.new.call(
      "GATEWAY_INTERFACE" => "CGI/1.1",
      "PATH_INFO" => "/index.html",
      "QUERY_STRING" => "",
      "REMOTE_ADDR" => "::1",
      "REMOTE_HOST" => "localhost",
      "REQUEST_METHOD" => "GET",
      "REQUEST_URI" => "http://localhost:3000/index.html",
      "SCRIPT_NAME" => "",
      "SERVER_NAME" => "localhost",
      "SERVER_PORT" => "3000",
      "SERVER_PROTOCOL" => "HTTP/1.1",
      "SERVER_SOFTWARE" => "WEBrick/1.3.1 (Ruby/2.0.0/2013-11-22)",
      "HTTP_HOST" => "localhost:3000",
      "HTTP_USER_AGENT" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:26.0) Gecko/20100101 Firefox/26.0",
      "HTTP_ACCEPT" => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
      "HTTP_ACCEPT_LANGUAGE" => "zh-tw,zh;q=0.8,en-us;q=0.5,en;q=0.3",
      "HTTP_ACCEPT_ENCODING" => "gzip, deflate",
      "HTTP_COOKIE" => "jsonrpc.session=3iqp3ydRwFyqjcfO0GT2bzUh.bacc2786c7a81df0d0e950bec8fa1a9b1ba0bb61",
      "HTTP_CONNECTION" => "keep-alive",
      "HTTP_CACHE_CONTROL" => "max-age=0",
      "rack.version" => [1, 2],
      "rack.input" => StringIO.new,
      "rack.errors" => STDERR,
      "rack.multithread" => true,
      "rack.multiprocess" => false,
      "rack.run_once" => false,
      "rack.url_scheme" => "http",
      "HTTP_VERSION" => "HTTP/1.1",
      "REQUEST_PATH" => "/index.html"
    )
    assert_instance_of Array, responce
    if Gem::Version.new(RUBY_VERSION) >= Gem::Version.new("2.4.0")
      assert_instance_of Integer, responce[0]
    else
      # rubocop:disable Lint/UnifiedInteger
      assert_instance_of Fixnum, responce[0]
      # rubocop:enable Lint/UnifiedInteger
    end
    assert responce[0] >= 100
    assert_instance_of Hash, responce[1]
    assert_instance_of Array, responce[2]
  end

  def test_404_error
    response = get("/ajeoifjaoigh")
    assert_equal "Error 404", response.body
    assert_equal 404, response.status
  end
end

class AlternateApp < ATD::App
  r "/", "Alternate declaration works!"
end

describe AlternateApp do
  include Rack::Test::Methods
  def app
    AlternateApp.new
  end

  def test_that_it_works
    assert_equal "Alternate declaration works!", get("/").body
  end
end

describe Name do
  include Rack::Test::Methods

  def app
    Name.new
  end

  def test_instance_methods_work
    refute_equal "This awesome syntax works!", get("/instance-test").body
  end
end

describe Name do
  include Rack::Test::Methods

  def app
    @app = AlternateApp.new
    @app.request "/instance-test", "This awesome syntax works!"
    @app
  end

  def test_instance_notation
    assert_equal "This awesome syntax works!", get("/instance-test").body
  end
end
