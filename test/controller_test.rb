require_relative "test_helper"

# rubocop:disable Style/ModuleFunction
module MyController
  extend self

  def whatever
    "Hi Folks!"
  end
end
# rubocop:enable Style/ModuleFunction

class Controller < ATD::App
  include MyController

  request "/controller-test", "MyController#whatever"
end

describe Controller do
  include Rack::Test::Methods

  def app
    Controller.new
  end

  def test_controller_works
    assert_equal "Hi Folks!", get("/controller-test").body
  end
end
