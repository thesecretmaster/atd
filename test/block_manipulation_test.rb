require_relative "test_helper"

class TestClass
  class <<self
    attr_accessor :get, :post, :put, :patch, :delete
  end
end

class Name
  r.get "/block-test", "hi" do
    TestClass.get = true
  end

  r.get "/editing-test", "Before" do
    view[:raw] = "#{view[:raw]} After"
  end

  r.post "/params-test" do
    view[:raw] = params["test"]
  end
end

describe ATD::App do
  include Rack::Test::Methods

  def app
    Name.new
  end

  def test_block
    get "/block-test"
    assert_equal true, TestClass.get
  end

  def test_recieve_params
    response = post("/params-test", "test" => "hi")
    assert_equal "hi", response.body
  end

  def test_block_output_editing
    assert_equal "Before After", get("/editing-test").body
  end
end
