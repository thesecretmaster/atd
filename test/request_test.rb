require_relative "test_helper"

class Name
  r.get.post.put.patch.delete "/method-addition" do
    view[:raw] = http[:method]
  end

  r "/all-methods" do
    view[:raw] = http[:method]
  end

  get.post.put.patch.delete "/regular-get" do
    view[:raw] = "regular #{http[:method].downcase} works!"
  end

  r.delete "/methods-hash", "test-methods-hash", respond_to: [:get, :put]

  r.get.get.post.put.put "/methods-test-1", "test-1", respond_to: :get

  r "/methods-test-2", "test-2", respond_to: [:put, :delete], ignore: [:patch, :put]

  r.get.post.patch "/methods-test-3", "test-3", ignore: :patch, respond_to: [:get, :post]
end

describe ATD::App do
  include Rack::Test::Methods

  def app
    Name.new
  end

  def test_verb_duplication
    assert_equal "test-1", get("/methods-test-1").body
    assert_equal "test-1", post("/methods-test-1").body
    assert_equal "test-1", put("/methods-test-1").body
    refute_equal "test-1", patch("/methods-test-1").body
    refute_equal "test-1", delete("/methods-test-1").body
  end

  def test_respond_to_ignore_conflict
    refute_equal "test-2", get("/methods-test-2").body
    refute_equal "test-2", post("/methods-test-2").body
    refute_equal "test-2", put("/methods-test-2").body
    refute_equal "test-2", patch("/methods-test-2").body
    assert_equal "test-2", delete("/methods-test-2").body
  end

  def test_ignore_override
    assert_equal "test-3", get("/methods-test-3").body
    assert_equal "test-3", post("/methods-test-3").body
    refute_equal "test-3", put("/methods-test-3").body
    refute_equal "test-3", patch("/methods-test-3").body
    refute_equal "test-3", delete("/methods-test-3").body
  end

  def test_request_creation
    assert_instance_of ATD::Route, req
    assert_instance_of ATD::Route, r
  end

  def test_method_addition
    assert_equal "GET", get("/method-addition").body
    assert_equal "POST", post("/method-addition").body
    assert_equal "PUT", put("/method-addition").body
    assert_equal "PATCH", patch("/method-addition").body
    assert_equal "DELETE", delete("/method-addition").body
  end

  def test_all_methods_routes
    assert_equal "get", get("/all-methods").body.downcase
  end

  def test_plain_get
    assert_equal "regular get works!", get("/regular-get").body
    assert_equal "regular delete works!", delete("/regular-get").body
    assert_equal "regular post works!", post("/regular-get").body
    assert_equal "regular put works!", put("/regular-get").body
    assert_equal "regular patch works!", patch("/regular-get").body
  end

  def test_methods_hash
    assert_equal "test-methods-hash", get("/methods-hash").body
    assert_equal "test-methods-hash", put("/methods-hash").body
    assert_equal "test-methods-hash", delete("/methods-hash").body
    refute_equal "test-methods-hash", post("/methods-hash").body
  end
end
