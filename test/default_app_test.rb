require_relative "test_helper"

r "/normal-route", "Norms are kewl"
req.get "/", "Gotten"
r.post "/", "Posted"
request.put "/", "Putsed"
r.delete "/", "Deleted"
r.patch "/", "Patched"

r "/ignore-test", "Ignore Works", ignore: [:get]

r "/return-val" do
  "Should return this" # should have return
end

get post put "/non-paren-syntax", "Non P Syn!"

r "/status-code", "Hello Status Codez", status_code: 146, status: 789

describe DefaultApp do
  include Rack::Test::Methods
  using ComparisonRefinements

  def app
    DefaultApp.new
  end

  def test_route_creation
    assert ATD::Route.new.get("/").same_properties_as? r.get("/")
  end

  def test_plain_routes
    assert_equal "Gotten", get("/").body
    assert_equal "Posted", post("/").body
    assert_equal "Deleted", delete("/").body
    assert_equal "Putsed", put("/").body
    assert_equal "Patched", patch("/").body
  end

  def test_ignore_option
    refute_equal "Ignore Works", get("/ignore-test").body
    assert_equal "Ignore Works", post("/ignore-test").body
  end

  def test_return_val
    assert_equal "Should return this", get("/return-val").body
  end

  def test_non_paren_syntax
    assert_equal "Non P Syn!", get("/non-paren-syntax").body
    assert_equal "Non P Syn!", post("/non-paren-syntax").body
    assert_equal "Non P Syn!", put("/non-paren-syntax").body
    refute_equal "Non P Syn!", patch("/non-paren-syntax").body
    refute_equal "Non P Syn!", delete("/non-paren-syntax").body
  end

  def test_normal_request
    assert_equal "Norms are kewl", get("/normal-route").body
    assert_equal "Norms are kewl", post("/normal-route").body
    assert_equal "Norms are kewl", put("/normal-route").body
    assert_equal "Norms are kewl", patch("/normal-route").body
    assert_equal "Norms are kewl", delete("/normal-route").body
    assert_equal 200, get("/normal-route").status
  end

  def test_change_status_codes
    assert_equal 146, get("/status-code").status
  end
end
