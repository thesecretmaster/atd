require_relative 'test_helper'

to_precompile "test" do
  "TEST"
end

to_compile "test_live" do
  "TEST_LIVE"
end

to_compile "prepend" do |file = ""|
  "Prep:#{file}"
end

class Name
  r "/precompilation-test", "test_file.test", compile: false

  r "/compilation-test-string", "test_file.test", precompile: false

  r "/live-compilation-test", "test_file.test_live", precompile: false

  r.get "/html-test", "test.html"

  r "/inline-comp" do
    test_live "test_file.test_live"
  end

  r "/live-comp-with-file", precompile: false do
    prepend "test_file.test"
  end
end

describe ATD::Compilation do
  include Rack::Test::Methods

  def app
    Name.new
  end

  def test_precomilation
    assert_equal "TEST", get("/precompilation-test").body
  end

  def test_live_compilation
    assert_equal "TEST_LIVE", get("/live-compilation-test").body
  end

  def test_read_html
    assert_equal File.read(File.expand_path(File.join(__dir__, "/assets/test.html"))), get("/html-test").body
    assert_equal "text/html", get("/html-test").header["content-type"]
  end

  def test_string_to_file
    assert_equal File.read(File.expand_path(File.join(__dir__, "/assets/test_file.test"))), get("/compilation-test-string").body
  end

  def test_inline_style
    assert_equal "TEST_LIVE", get("/inline-comp").body
  end

  def test_inline_compilation_with_file
    # rubocop:disable Style/StringLiteralsInInterpolation
    assert_equal "Prep:#{File.read(File.expand_path(File.join(__dir__, "/assets/test_file.test")))}", get("/live-comp-with-file").body
    # rubocop:enable Style/StringLiteralsInInterpolation
  end
end
