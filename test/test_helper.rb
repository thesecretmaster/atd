$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
ENV['RACK_ENV'] = 'test'
require 'simplecov'
SimpleCov.start do
  add_filter "/test/"
end
require 'atd'
require 'minitest/autorun'
require 'minitest/assertions'
require 'minitest/reporters'
require 'rack/test'

MiniTest::Reporters.use!

module ComparisonRefinements
  refine Object do
    # Checks if two objects are instances of the same class and that they have the same instance variables
    def same_properties_as?(other_class)
      other_class.class == self.class && instance_variables == other_class.instance_variables
    end
  end
end

class Name < ATD::App
end
