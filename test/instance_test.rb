require_relative "test_helper"

class Name
  attr_accessor :my_name

  request "/test-manipulation", "Hi! This is my_name's App!" do
    view[:raw] = view[:raw].gsub("my_name", @my_name.to_s)
  end
end

describe Name do
  include Rack::Test::Methods
  def app
    a = Name.new
    a.my_name = "Fredrick"
    a
  end

  def test_string_manipulation
    assert_equal "Hi! This is Fredrick's App!", get("/test-manipulation").body
  end
end
